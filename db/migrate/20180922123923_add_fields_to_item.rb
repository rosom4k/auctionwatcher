class AddFieldsToItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :auction_count, :integer, default: 0
    add_column :items, :lowest_price, :bigint, default: 0
  end
end
