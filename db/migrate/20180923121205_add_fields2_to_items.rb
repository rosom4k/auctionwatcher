class AddFields2ToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :itemClass, :integer, default: nil
    add_column :items, :itemSubClass, :integer, default: nil
    add_column :items, :itemLevel, :integer, default: nil

    remove_column :items, :auction_count
    remove_column :items, :lowest_price
  end
end
