class CreateTimestamps < ActiveRecord::Migration[5.2]
  def change
    create_table :timestamps do |t|
      t.bigint :timestamp

      t.timestamps
    end

    add_foreign_key :auctions, :timestamps
  end
end
