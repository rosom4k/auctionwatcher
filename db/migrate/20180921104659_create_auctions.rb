class CreateAuctions < ActiveRecord::Migration[5.2]
  def change
    create_table :auctions do |t|

      t.bigint :auc
      t.integer :item
      t.string :owner
      t.bigint :bid
      t.bigint :buyout
      t.integer :quantity
      t.string :timeLeft
      #t.bigint :rand
      #t.integer :seed
      t.string :context
      #t.string :ownerRealm
      t.string :bonusLists
      t.string :modifiers
      #t.string :petSpeciesId
      #t.string :petBreedId
      #t.string :petLevel
      #t.string :petQualityId
      t.integer :timestamp_id
      
      t.timestamps
    end
  end
end
