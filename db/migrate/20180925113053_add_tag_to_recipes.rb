class AddTagToRecipes < ActiveRecord::Migration[5.2]
  def change
    add_column :recipes, :tag, :string
  end
end
