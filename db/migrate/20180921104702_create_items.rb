class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|

      t.integer :item_id, null: false
      t.string :name, default: ''
      t.string :icon, default: ''

      t.timestamps
    end
    add_index :items, :item_id, unique: true
    add_index :items, :id, unique: true
    add_index :items, :name, unique: false
  end
end
