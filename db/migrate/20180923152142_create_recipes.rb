class CreateRecipes < ActiveRecord::Migration[5.2]
  def change
    create_table :recipes do |t|
      t.string :name
      t.integer :item_id
      t.timestamps
    end

    create_table :ingredients do |t|
      t.integer :item_id
      t.integer :recipe_id
      t.integer :count
    end

    add_foreign_key :ingredients, :recipes
    add_foreign_key :ingredients, :items
    add_foreign_key :recipes, :items

  end
end
