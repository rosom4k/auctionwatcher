class AddTimestampHistory < ActiveRecord::Migration[5.2]
  def change
    create_table :timestamp_histories do |t|
      t.bigint :mean_price
      t.integer :auction_count
      t.integer :item_count
      t.integer :timestamp_id
      t.integer :item_id
      t.string :notable_sellers

      t.timestamps
    end

    add_foreign_key :timestamp_histories, :items
    add_foreign_key :timestamp_histories, :timestamps
  end
end
