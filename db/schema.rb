# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_28_154312) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "auctions", force: :cascade do |t|
    t.bigint "auc"
    t.integer "item"
    t.string "owner"
    t.bigint "bid"
    t.bigint "buyout"
    t.integer "quantity"
    t.string "timeLeft"
    t.string "context"
    t.string "bonusLists"
    t.string "modifiers"
    t.integer "timestamp_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ingredients", force: :cascade do |t|
    t.integer "item_id"
    t.integer "recipe_id"
    t.integer "count"
  end

  create_table "items", force: :cascade do |t|
    t.integer "item_id", null: false
    t.string "name", default: ""
    t.string "icon", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "itemClass"
    t.integer "itemSubClass"
    t.integer "itemLevel"
    t.index ["id"], name: "index_items_on_id", unique: true
    t.index ["item_id"], name: "index_items_on_item_id", unique: true
    t.index ["name"], name: "index_items_on_name"
  end

  create_table "recipes", force: :cascade do |t|
    t.string "name"
    t.integer "item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tag"
  end

  create_table "timestamp_histories", force: :cascade do |t|
    t.bigint "mean_price"
    t.integer "auction_count"
    t.integer "item_count"
    t.integer "timestamp_id"
    t.integer "item_id"
    t.string "notable_sellers"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "timestamps", force: :cascade do |t|
    t.bigint "timestamp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "auctions", "timestamps"
  add_foreign_key "ingredients", "items"
  add_foreign_key "ingredients", "recipes"
  add_foreign_key "recipes", "items"
  add_foreign_key "timestamp_histories", "items"
  add_foreign_key "timestamp_histories", "timestamps"
end
