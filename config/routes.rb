Rails.application.routes.draw do

  resources :recipes
  root to: 'items#index'


  resources :items, except: [:create, :delete, :update] do
    get 'item_auctions_data'
  end
  resources :auctions, except: [:create, :delete, :update]

# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
