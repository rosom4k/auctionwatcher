API_KEY_WITH_LOCALE  = '?locale=en_GB&apikey=74q3bcrtmzzxf3mxqz7bgfrmcz8nv6n2'
ITEM_API_SRC   = 'https://eu.api.battle.net/wow/item/'
AUCTIONS_API_SRC = 'https://eu.api.battle.net/wow/auction/data/burning%20legion'

VALID_AUCTION_FIELDS = %w(item owner bid buyout quantity timeLeft context bonusLists)
