class ItemWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'item'
  def perform(item_id)
    item = Item.where(item_id: item_id).first
    if item.blank?
      item = Item.new(item_id: item_id)
      item.save
      item.update_item_data
    elsif item.name.blank? || item.itemLevel.blank?
      item.update_item_data
    end
  end
end

