class AuctionWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'auction'
  
  def perform(auction_data, timestamp_id)
    return if auction_data['buyout'].to_i == 0
    record = Auction.new

    VALID_AUCTION_FIELDS.each{|f| record[f] = auction_data[f]}
    record.timestamp_id = timestamp_id
    record.save

  end
end
