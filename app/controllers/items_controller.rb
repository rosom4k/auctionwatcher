class ItemsController < ApplicationController
  before_action :set_item, only: [:show]

  # GET /items
  # GET /items.json
  def index
    @items = items
    @t_id = Timestamp.order(:created_at).last.id
    @last_search = params[:item][:name] rescue ''
  end

  # GET /items/1
  # GET /items/1.json
  def show
    @auctions = []
    @t_id = Timestamp.order(:created_at).last.id
    raw_auctions = @item.auctions(Timestamp.order(:created_at).last.id)
      .map{|q| q.without_item}

    raw_auctions.each do |auction|
      if !@auctions.include?({count: raw_auctions.count(auction), auction: auction})
        @auctions << {count: raw_auctions.count(auction), auction: auction} 
      end
    end

    @auctions.sort!{|b, a| a[:auction][:buyout_per_unit] <=> b[:auction][:buyout_per_unit]}
  end

  def item_auctions_data
    item = Item.find(params[:item_id])

    if params[:timestamp_id]
      timestamp_id = params[:timestamp_id]
    else
      timestamp_id = Timestamp.order(:created_at).last.id
    end

    data = auction_statistic_data(timestamp_id)
    render json: data
  end


  private
    # Use callbacks to share common setup or constraints between actions.

    def items
      page_param = set_pagination_param
      item = params[:item]

      if item
        query = Item.where("name ~* ?", item[:name].strip)
      else
        query = Item.where.not(name: '')
      end
        query = query.where(itemLevel: (100..1000))

      query.page(page_param).per(32)
    end

    def set_item
      @item = Item.find(params[:id])
    end

    def set_pagination_param
      unless params[:page].blank?
        params[:page].to_i > 0 ? params[:page].to_i : 0 
      else
        1
      end
    end

    def separated_price(price)
      return {} if price.nil?
      price = price.round(0)
      copper = price % 100
      silver = (price % 10000 - copper) / 100
      gold = (price - silver*100 - copper) / 10000
      return { 
        'copper': copper.round(1), 
        'silver': silver.round(1), 
        'gold': gold.round(1)
      }
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.fetch(:item, {})
    end
end
