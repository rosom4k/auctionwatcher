class AuctionsController < ApplicationController
  before_action :set_auction, only: [:show, :edit, :update, :destroy]

  # GET /auctions
  # GET /auctions.json
  def index
    @auctions = auctions
    @last_search = params[:auction][:owner] rescue ''
  end

  # GET /auctions/1
  # GET /auctions/1.json
  def show
  end

  private

    def auctions
      page_param = set_pagination_param
      auction = params[:auction]

      if auction
        query = Auction.where("owner ~* ?", auction[:owner].strip)
        query2 = query.where(timestamp: Timestamp.order(:created_at).last)
        if query2.count == 0 && Timestamp.count > 1
          query2 = query.where(timestamp: Timestamp.order(:created_at).all[-2])
        end
        query = query2
      else
        query = Auction.all
      end

      query.page(page_param).per(64)
    end
    # Use callbacks to share common setup or constraints between actions.

    def set_pagination_param
      unless params[:page].blank?
        params[:page].to_i > 0 ? params[:page].to_i : 0 
      else
        1
      end
    end

    def set_auction
      @auction = Auction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def auction_params
      params.fetch(:auction, {})
    end
end
