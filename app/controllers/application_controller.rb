class ApplicationController < ActionController::Base
  def set_pagination_param
    unless params[:page].blank?
      params[:page].to_i > 0 ? params[:page].to_i : 0 
    else
      1
    end
  end
end
