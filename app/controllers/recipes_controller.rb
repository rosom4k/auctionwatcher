class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy]

  # GET /recipes
  def index
    @t_id = Timestamp.order(:created_at).last.id
    @recipes = recipes
  end

  # GET /recipes/1
  def show
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
  end

  # GET /recipes/1/edit
  def edit
  end

  # POST /recipes
  def create
    @recipe = Recipe.new(recipe_params)
    asd = params
    if @recipe.save
      params['recipe']['ingredients'].each do |id|
        next if id.blank?
        @recipe.ingredients.push(Ingredient.new(item_id: id))
      end
      redirect_to @recipe, notice: 'Recipe was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /recipes/1
  def update
    @ingredient = Ingredient.find(ingredient_params['id']) rescue nil
    @recipe = Recipe.find(recipe_params['id']) rescue nil
    puts params
    render json: {status: 'error'} and return if @ingredient.nil? && @recipe.nil?

    @ingredient.update(ingredient_params) if !@ingredient.nil?
    @recipe.update(recipe_params) if !@recipe.nil?

    render json: {status: 'success'}
  end

  # DELETE /recipes/1
  def destroy
    @recipe.destroy
    redirect_to recipes_url, notice: 'Recipe was successfully destroyed.'
  end

  private
    def recipes
      page_param = set_pagination_param
      recipe = params[:recipe]

      if recipe
        query = Recipe.where("name ~* ?", recipe[:name].strip)
      else
        query = Recipe.where.not(name: '')
      end

      query#.page(page_param).per(32)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def recipe_params
      params.require(:recipe).permit(:id, :name, :item_id, :tag)
    end

    def ingredient_params
      params.require(:ingredient).permit(:count, :id)
    end
end
