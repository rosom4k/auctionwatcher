class Item < ApplicationRecord
  include ActionView::Helpers

  validates :item_id, uniqueness: true
  has_many :timestamp_histories, dependent: :destroy

  def auctions(timestamp_id)
    auctions = Auction.where(item: self.item_id, timestamp_id: timestamp_id)
    auctions = [] unless auctions
    auctions
  end
  
  def update_item_data
    require 'open-uri'
    begin
      item_raw_data = open(item_api_src(self.item_id)).read
      item_data = JSON.parse(item_raw_data)
      self.name = item_data['name']
      self.itemLevel = item_data['itemLevel']
      self.itemClass = item_data['itemClass']
      self.itemSubClass = item_data['itemSubClass']
      self.icon = item_data['icon']
      self.save
    rescue 
      nil
    end
  end

  def item_api_src(item_id)
    ITEM_API_SRC + item_id.to_s + API_KEY_WITH_LOCALE
  end

  def self.update_missing_items
    Auction.distinct.pluck(:item).each do |item_id|
      item = Item.where(item_id: item_id).first
      if item.nil?
        Item.create(item_id: item_id)
      end
    end

    Item.where(name: '').each{|i| ItemWorker.perform_async(i.id)}
  end

  def auction_statistic_data(timestamp_id)
    data = {}
    sorted_auctions = self.auctions(timestamp_id).map{|q| q.without_item}.sort{|b, a| a[:buyout_per_unit] <=> b[:buyout_per_unit]}
    return data if sorted_auctions.empty?
    lowest_price = sorted_auctions.first[:buyout_per_unit]
    items_count = sorted_auctions.map{|q| q[:quantity]}.sum
    lower_count = items_count / 10
    lower_count = 1 if lower_count == 0
    
    lower_mean_price = 0
    tmp_count = 0
    sorted_auctions.each do |a|
      tmp_count += a[:quantity]
      lower_mean_price += a[:buyout]
      break if tmp_count >= lower_count
    end

    lower_mean_price = lower_mean_price / tmp_count



    data[:auction_count] =  sorted_auctions.count
    data[:lowest_price] = separated_price(lowest_price)
    data[:mean_price] = separated_price(lower_mean_price)
    data[:item_count] = items_count
    data[:updated_at] = self.auctions(timestamp_id).order(:updated_at).last.updated_at.strftime('%m-%d-%Y %I:%M%p') rescue '---'
    data
  end

  def separated_price(price)
    return {} if price.nil?
    price = price.round(0)
    copper = price % 100
    silver = (price % 10000 - copper) / 100
    gold = (price - silver*100 - copper) / 10000
    return { 
      'copper': copper.round(1), 
      'silver': silver.round(1), 
      'gold': gold.round(1)
    }
  end
  
end



