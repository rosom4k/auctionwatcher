class Timestamp < ApplicationRecord
  validates :timestamp, presence: true
  has_many :auctions, dependent: :destroy
  has_many :timestamp_histories, dependent: :destroy

  def old_clean_auctions

    if (Time.now.to_i - self.timestamp)/60 >= 2880 # 48 * 60
      self.auctions.where(timeLeft: 'SHORT').each{|q| q.destroy}
      self.auctions.where(timeLeft: 'MEDIUM').each{|q| q.destroy}
      self.auctions.where(timeLeft: 'LONG').each{|q| q.destroy}
      self.auctions.where(timeLeft: 'VERY_LONG').each{|q| q.destroy}
    elsif (Time.now.to_i - self.timestamp)/60 >= 720  # 12 * 60
      self.auctions.where(timeLeft: 'SHORT').each{|q| q.destroy}
      self.auctions.where(timeLeft: 'MEDIUM').each{|q| q.destroy}
      self.auctions.where(timeLeft: 'LONG').each{|q| q.destroy}
    elsif (Time.now.to_i - self.timestamp)/60 >= 120
      self.auctions.where(timeLeft: 'SHORT').each{|q| q.destroy}
      self.auctions.where(timeLeft: 'MEDIUM').each{|q| q.destroy}
    elsif (Time.now.to_i - self.timestamp)/60 >= 30
      self.auctions.where(timeLeft: 'SHORT').each{|q| q.destroy}
    end

  end

  def clean
    if (Time.now.to_i - self.timestamp)/60 >= 60 * 6 * 1 # 6 hours
      Item.all.each do |timestamp_item|
        data = timestamp_item.auction_statistic_data(self.id)
        next if data == {}
        self.timestamp_histories.push(
          TimestampHistory.new(
            mean_price: data[:mean_price][:gold]*1000 + 
              data[:mean_price][:silver]*10 +
              data[:mean_price][:copper],
            auction_count: data[:auction_count],
            item_count: data[:item_count],
            item_id: timestamp_item.id
          )
        )
        timestamp_item.auctions(self.id).each{|a| a.destroy}
      end
    end
  end

  def self.clean_all
    Timestamp.all.each{|t| t.clean if t.timestamp_histories.count > 0}
  end

end