class Auction < ApplicationRecord
  # validates :auc, uniqueness: true

  # validates :bid, exclusion: { in: %w(0) }
  # validates :buyout, exclusion: { in: %w(0) }
  # validates :quanity, exclusion: { in: %w(0) }
  belongs_to :timestamp

  def without_item
    json = {}
    VALID_AUCTION_FIELDS.each{|vk| json[vk.to_sym] = self[vk]}
    json[:buyout_per_unit] = json[:buyout].to_f / json[:quantity].to_f
    json
  end

  def self.get_auctions_api
    json_response = JSON.parse(open(AUCTIONS_API_SRC + API_KEY_WITH_LOCALE).read)
  end

  def self.get_auctions
    require 'open-uri'
    auctions_api = self.get_auctions_api
    timestamp = auctions_api['files'].first['lastModified'].to_f/1000
    auction_house_data_src = auctions_api['files'].first['url']
    json_response = JSON.parse(open(auction_house_data_src).read)

    item_ids = Item.where(itemLevel: (100..5000)).pluck(:item_id)
    self.add_records(json_response['auctions'].select{|i| item_ids.include?(i['item'])}, timestamp)
  end

  def self.actualize_items
    require 'sidekiq'
    Auction.all.each_with_index do |a, i|
      ItemWorker.perform_async(a.item)
    end
  end

  private

  def self.add_records(auctions, t)
    last_timestamp = Timestamp.where(timestamp: t).first
    if last_timestamp
      return nil
    else
      ts = Timestamp.new(timestamp: t)
      ts.save
      ts_id = ts.id
    end
    puts "Found #{auctions.count} auctions..."
    auctions.each do |auction_json|
      AuctionWorker.perform_async(auction_json, ts_id)
    end
  end

  # auction timeLeft:
  #
  # very long: 12  - 48h
  # long:      2   - 12h
  # medium:    0.5 - 2h
  # short:     0   - 0.5h

end
