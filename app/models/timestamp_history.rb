class TimestampHistory < ApplicationRecord
  has_many :timestamps
  has_many :items
end

