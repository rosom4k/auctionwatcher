module ApplicationHelper

  def queue_size
    require 'sidekiq/api'
    Sidekiq::Queue.all.map{|q| q.size}.sum
  end

  def separated_price(price)
    return {} if price.nil?
    price = price.round(0)
    copper = price % 100
    silver = (price % 10000 - copper) / 100
    gold = (price - silver*100 - copper) / 10000
    return { 
      'copper': copper.round(1), 
      'silver': silver.round(1), 
      'gold': gold.round(1)
    }
  end

end
