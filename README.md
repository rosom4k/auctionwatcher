# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 
`2.5.1`

* System dependencies: 
`Tested and deployed on Debian Jessie 64x`

* Configuration

* Database creation
`postgresql`

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)
`sidekiq`, `redis`
* Deployment instructions

* Routine
every 30 minutes:

to add current auctions and update item info
`Auction.get_auctions`
